import alblogs
import logging
import os.path
import datetime
import re
import gzip
from urllib.parse import urlparse, parse_qs
import yaml

LOGGER = None

# old LOG_ENTRY_RE = re.compile("(.*?) (.*?) (.*?) (.*?)\:(.*?) (.*?)\:(.*?) (.*?) (.*?) (.*?) (.*?) (.*?) (.*?) (.*?) \"(.*?) (.*?) (.*?)\" \"(.*?)\" (.*?) (.*?) (.*?) \"(.*?)\" \"(.*?)\" \"(.*?)\" (.*?) (.*?) \"(.*?)\"")

ALB_LOG_ENTRY_RE = re.compile('(.*?) (.*?) (.*?) (.*?) (.*?) (.*?) (.*?) (.*?) (.*?) (.*?) (.*?) (.*?) "(.*?) (.*?) (.*?)" "(.*?)" (.*?) (.*?) (.*?) "(.*?)" "(.*?)" "(.*?)" (.*?) (.*?) "(.*?)" "(.*?)" "(.*?)"')

CLB_LOG_ENTRY_RE = re.compile(
    '(.*?) (.*?) (.*?) (.*?) (.*?) (.*?) (.*?) (.*?) (.*?) (.*?) (.*?) "(.*?) (.*?) (.*?)" "(.*?)" (.*?) (.*?)')
#      1     2     3     4     5     6     7     8     9    10    11     12    13    14      15     16    17

# 2018-07-24T13:25:00.488400Z
DATE_RE = re.compile("(.*?)T(\d\d)\:(\d\d)\:(\d\d)\.(\d+)Z")
IP_PORT_RE = re.compile("(.*):(.*)")


def find_logs(current_path, logfiles):
    with os.scandir(current_path) as it:
        for entry in it:
            log_path = os.path.join(current_path, entry.name)
            if entry.is_file():
                logfiles.append(log_path)
                LOGGER.debug("Found log file '{}'".format(log_path))
            elif entry.is_dir():
                find_logs(log_path, logfiles)


def fix_negative(value):
    if float(value) < 0:
        return 0

    return float(value)


def parse_request(request):
    url = None
    query = None

    parts = request.split("?")
    url = parts[0]

    parse_result = urlparse(request)
    netloc = parse_result.netloc
    uri = parse_result.path
    query = parse_result.query
    if len(query.strip()) == 0:
        query = None

    return url, uri, query, netloc


def parse_date(datestr):
    datepart = None
    hourpart = None
    minutepart = None
    secondspart = None
    microsecondspart = None

    match = DATE_RE.match(datestr)
    if match:
        datepart = match.group(1).strip()
        hourpart = match.group(2).strip()
        minutepart = match.group(3).strip()
        secondspart = match.group(4).strip()
        microsecondspart = match.group(5).strip()

    else:
        raise RuntimeError("DATE NOMATCH: {0}".format(datestr))

    time_hour = "{}:00".format(hourpart)
    time_minute = "{}:{}".format(hourpart, minutepart)
    time_second = "{}:{}:{}".format(hourpart, minutepart, secondspart)

    return datepart, hourpart, minutepart, secondspart, microsecondspart, time_hour, time_minute, time_second


def parse_address(address):
    match = IP_PORT_RE.match(address)
    if match:
        return match.group(1).strip(), match.group(2).strip()
    else:
        return None, None


def parse_alb_entry(line, tg_mappings, ca_mappings, request_cache):

    record = None

    match = ALB_LOG_ENTRY_RE.match(line)
    if match:
        request_str = match.group(14).strip()
        if request_str in request_cache:
            (request_url, request_uri, request_query, netloc) = request_cache[request_str]
        else:
            (request_url, request_uri, request_query, netloc) = parse_request(request_str)
            request_cache[request_str] = (request_url, request_uri, request_query, netloc)

        (datepart, hourpart, minutepart, secondspart, microsecondspart, time_hour, time_minute, time_second) = parse_date(match.group(2).strip())

        request_processing_time = fix_negative(match.group(6).strip())
        target_processing_time = fix_negative(match.group(7).strip())
        response_processing_time = fix_negative(match.group(8).strip())

        (client_ip, client_port) = parse_address(match.group(4))
        (target_ip, target_port) = parse_address(match.group(5))

        target_group_arn = match.group(19).strip()
        target_group_name = tg_mappings.get(target_group_arn)
        if target_group_name is None:
            target_group_name = "unmapped"

        client_ip_description = ca_mappings.get(client_ip)
        if client_ip_description is None:
            client_ip_description = "unmapped"

        dt_timestamp = datetime.datetime.strptime(match.group(2).strip(), '%Y-%m-%dT%H:%M:%S.%f%z')
        dt_creation = datetime.datetime.strptime(match.group(24).strip(), '%Y-%m-%dT%H:%M:%S.%f%z')
        delta_time = dt_timestamp - dt_creation
        total_time = delta_time.seconds + (delta_time.microseconds / 1000000)

        record = {"type": match.group(1).strip()
            , "timestamp": match.group(2).strip()
            , "datepart": datepart
            , "hourpart": hourpart
            , "minutepart": minutepart
            , "secondspart": secondspart
            , "microsecondspart": microsecondspart
            , "time_hour": time_hour
            , "time_minute": time_minute
            , "time_second": time_second
            , "elb": match.group(3).strip()
            , "client_ip": client_ip
            , "client_ip_description": client_ip_description
            , "client_port": client_port
            , "target_ip": target_ip
            , "target_port": target_port
            , "request_processing_time": request_processing_time
            , "target_processing_time": target_processing_time
            , "response_processing_time": response_processing_time
            , "elb_status_code": match.group(9).strip()
            , "target_status_code": match.group(10).strip()
            , "received_bytes": match.group(11).strip()
            , "sent_bytes": match.group(12).strip()
            , "request_method": match.group(13).strip()
            , "request_url": request_url
            , "request_uri": request_uri
            , "request_query": request_query
            , "http_version": match.group(15).strip()
            , "user_agent": match.group(16).strip()
            , "ssl_cipher": match.group(17).strip()
            , "ssl_protocol": match.group(18).strip()
            , "target_group_arn": target_group_arn
            , "target_group_name": target_group_name
            , "trace_id": match.group(20).strip()
            , "domain_name": match.group(21).strip()
            , "chosen_cert_arn": match.group(22).strip()
            , "matched_rule_priority": match.group(23).strip()
            , "request_creation_time": match.group(24).strip()
            , "actions_executed": match.group(25).strip()
            , "redirect_url": match.group(26).strip()
            , "error_reason": match.group(27).strip()
            , "total_time": total_time
            , "netloc": netloc
                  }

    else:
        LOGGER.warning("NOMATCH: {}".format(line))

    return record


def parse_clb_entry(line):
    record = None

    match = CLB_LOG_ENTRY_RE.match(line)
    if match:
        (request_url, request_uri, request_query, netloc) = parse_request(match.group(13).strip())
        (datepart, hourpart, minutepart, secondspart, microsecondspart, time_hour, time_minute, time_second) = parse_date(match.group(1).strip())

        request_processing_time = fix_negative(match.group(5).strip())
        target_processing_time = fix_negative(match.group(6).strip())
        response_processing_time = fix_negative(match.group(7).strip())
        total_time = 0

        (client_ip, client_port) = parse_address(match.group(3))
        (target_ip, target_port) = parse_address(match.group(4))

        record = {"type": "clb-dummy"
            , "timestamp": match.group(1).strip()
            , "datepart": datepart
            , "hourpart": hourpart
            , "minutepart": minutepart
            , "secondspart": secondspart
            , "microsecondspart": microsecondspart
            , "time_hour": time_hour
            , "time_minute": time_minute
            , "time_second": time_second
            , "elb": match.group(2).strip()
            , "client_ip": client_ip
            , "client_ip_description": "clb-dummy"
            , "client_port": client_port
            , "target_ip": target_ip
            , "target_port": target_port
            , "request_processing_time": request_processing_time
            , "target_processing_time": target_processing_time
            , "response_processing_time": response_processing_time
            , "elb_status_code": match.group(8).strip()
            , "target_status_code": match.group(9).strip()
            , "received_bytes": match.group(10).strip()
            , "sent_bytes": match.group(11).strip()
            , "request_method": match.group(12).strip()
            , "request_url": request_url
            , "request_uri": request_uri
            , "request_query": request_query
            , "http_version": match.group(14).strip()
            , "user_agent": match.group(15).strip()
            , "ssl_cipher": match.group(16).strip()
            , "ssl_protocol": match.group(17).strip()
            , "target_group_arn": "clb-dummy"
            , "target_group_name": "clb-dummy"
            , "trace_id": "clb-dummy"
            , "domain_name": "clb-dummy"
            , "chosen_cert_arn": "clb-dummy"
            , "matched_rule_priority": "clb-dummy"
            , "request_creation_time": "clb-dummy"
            , "actions_executed": "clb-dummy"
            , "redirect_url": "clb-dummy"
            , "total_time": total_time
            , "error_reason": "clb-dummy"
            , "netloc": netloc
                  }

    else:
        LOGGER.warning("NOMATCH: {}".format(line))

    return record


def parse_logfile(db, logname, aws_config, tg_mappings, ca_mappings, request_cache):
    LOGGER.info("Processing: '{}'".format(logname))

    log_source_id = db.add_source(logname, aws_config.get_profile(), aws_config.get_bucket_name(), aws_config.get_base_dir(), aws_config.get_log_format())

    if logname.endswith(".gz"):
        LOGGER.debug("Log file is a gzip file")
        fhandle = gzip.open(logname, "rt", encoding="latin-1")
    else:
        LOGGER.debug("Log file is a normal text file file")
        fhandle = open(logname, "r", encoding="latin-1")

    count = 0
    for line in fhandle:
        count += 1
        if count % 1000 == 0:
            LOGGER.debug("Processed {} lines".format(count))

        line = line.rstrip()

        record = None
        if aws_config.get_log_format() == "alb":
            record = parse_alb_entry(line, tg_mappings, ca_mappings, request_cache)
        elif aws_config.get_log_format() == "clb":
            record = parse_clb_entry(line)
        else:
            raise RuntimeError("Unknown log format '{}', only 'alb' and 'clb' are supported".format(aws_config.get_log_format()))

        if record is not None:
            db.save_record(log_source_id, record)

    LOGGER.info("Processed {} lines".format(count))

    db.commit()


def read_arguments():
    argparser = alblogs.get_default_argparser()
    argparser.add_argument("date", metavar="DATE", type=str, help="Date for which to process ALB logs (yyyy-mm-dd)")

    args = argparser.parse_args()

    # try to convert args.date to check if it is a valid date
    datetime.datetime.strptime(args.date, '%Y-%m-%d')

    return args

# ------------------------------ MAIN PROGRAM ------------------------------


args = read_arguments()

alblogs.initialize(args)

LOGGER = logging.getLogger(__name__)
LOGGER.info("Starting parse_logs.py")

config = alblogs.get_configuration()

mappings_dir = config.get_mappings_dir()

tg_mappings = {}
ca_mappings = {}

if mappings_dir is not None:
    tg_file = os.path.join(mappings_dir, "target_group.yaml")
    if os.path.exists(tg_file):
        with open(tg_file, 'r') as fhandle:
            tg_mappings = yaml.load(fhandle, Loader=yaml.FullLoader)

    ca_file = os.path.join(mappings_dir, "client_address.yaml")
    if os.path.exists(ca_file):
        with open(ca_file, 'r') as fhandle:
            ca_mappings = yaml.load(fhandle, Loader=yaml.FullLoader)

source_dir = "{}/{}".format(config.get_srclogs_dir(), args.date)
if not os.path.exists(source_dir):
    raise RuntimeError("ALB log source directory '{}' does not exist".format(source_dir))

dbfile = "{}/{}-alblogs.db".format(config.get_data_dir(), args.date)

#dbfile = "{}/alblogs{}.db".format(config.get_data_dir(), "2930")

if os.path.exists(dbfile):
    raise RuntimeError("Database '{}' already exists".format(dbfile))

data_dir, dbname = os.path.split(dbfile)
if not os.path.exists(data_dir):
    os.makedirs(data_dir)

db = alblogs.open_logsdb(dbfile, create=True)

logfiles = []
find_logs(source_dir, logfiles)

request_cache = {}
for logfile in logfiles:
    parse_logfile(db, logfile, config.aws, tg_mappings, ca_mappings, request_cache)
